在和一位名叫後部有人的日本人組隊，並首度在其宿舍過夜的當晚，艾莉緹亞・聖特雷爾與珠洲菜搬到比兩人原本住處還高檔許多的『套房』，並為了能睽違已久地享受寬敞的浴室感到有些感動。

「呵呵⋯⋯艾莉小姐看起來好像很高興呢。我也很感動這裡竟然有這麼大的浴室。」
「才、才沒那回事⋯⋯不，我確實是有點開心啦。但以我待在珠洲菜宿舍裡白吃白喝的身分，是無法要求太多的。」

艾莉緹亞原本住在五號區，而非剛來到迷宮國的初學者們所生活的八號區，卻因故來到了這裡。由於八號區沒有多餘的宿舍，因此她昨天在符合珠洲菜探索者排名、位於食堂街的公寓借住一晚。

然而公寓裡沒有浴室，只在簡樸的屋檐下，放著以汽油桶做成的浴缸。雖然艾莉緹亞和珠洲菜洗澡時輪流把風，但珠洲菜的等級還只有１，因此艾莉緹亞洗澡時不得不提心吊膽，一面擔心珠洲菜的安危，一面隨時準備在可疑人士出沒時將其擊退。

要是犯下偷窺等罪行，『業力值』就會上升，衛兵也會立刻趕來抓人。即使如此，仍無法完全遏止犯罪行為，加上迷宮國每種職業的特殊技能多如繁星，無論如何都會出現濫用技能進行犯罪的人。

（不過⋯⋯和我們比起來，這些人應該更困擾吧。她們到底是吃了什麼，胸部才會變得那麼大呢⋯⋯？而且腰還那麼細。）

艾莉緹亞看向一起過來洗澡的兩位女性──鏡花和露易莎。

「呼⋯⋯沐浴果然能洗滌人心呢。」
「五十嵐小姐，要不要幫您刷刷背呢？看您感覺還滿累的。」
「不、不用啦⋯⋯露易莎小姐工作了一天，應該也累了吧。我才想問您，要是不介意的話，需不需要我幫忙刷背呢。」
「是啊，我確實⋯⋯肩頸有點僵硬呢。不過在迷宮國很難買到尺寸剛好的內衣，所以我都很努力不讓胸部大小有所改變。」
「就是說呀⋯⋯提到這個，我也覺得有點郁悶。整個迷宮國難道就沒有幾個販賣優質內衣的地區嗎？」
「似乎各有幾位從事『製衣匠』和『裁縫師』等職業的人，不過也有人開宗明義就是『內衣設計師』喔。」
「咦？太強了⋯⋯那一定代表他們直接把前世的職業寫下來了吧。不曉得能不能利用技能來製作內衣呢？」
「我想是這樣沒錯。儘管他們應該也有能在探索或戰鬥時使用的技能，不過他們究竟是如何戰鬥的，這一點在公會的女孩子們之間曾蔚為話題呢。」

看來在酒館時總是互相牽制的兩人，不知從何時開始已經打成一片。就連不諳男女之事的艾莉緹亞和珠洲菜，都能一眼看出她們只要在人前就會產生敵對意識。

雖然艾莉緹亞和珠洲菜的發育狀況可說與年齡相符，然而看著擁有Ｇ罩杯以上傲人雙峰的鏡花和露易莎，仍忍不住面面相覷。

「不過⋯⋯五十嵐小姐的胸部真的很大，而且形狀也很漂亮呢。我認為日本人很少能有如此豐滿的上圍。」
「我原本以為這樣戰鬥起來會很不方便，但不知是否因為當上了『女武神』⋯⋯即使戰鬥時活動身體，胸部也不太會覺得痛，真是太好了。難道這都是拜魔力之賜嗎？」
「我想正是這樣沒錯，不過還是要注意別讓胸部大幅度搖晃比較好呢，畢竟一旦下垂，就無法恢復原狀了。」
「⋯⋯你、你說下垂，這也太糟了⋯⋯就沒有能讓胸部恢復原本模樣的技能嗎⋯⋯」
「或許有，但具體而言究竟是怎樣的技能，恕我孤陋寡聞，所以並不清楚。雖說似乎存在著『豐胸按摩』這種技能⋯⋯順道一提，技能的擁有者是女性。」

艾莉緹亞和珠洲菜一邊聽著兩人的對話，一邊坐在洗澡凳上有意無意地觸摸自己的胸部──接著又對彼此想著同一件事而互相露出苦笑。

「⋯⋯胸部大的人也有很多辛苦之處呢⋯⋯我們這個大小就夠了啦。」
「是、是啊⋯⋯」

珠洲菜感覺艾莉緹亞對此有些在意，但還是紅著臉如此回答。

之後年長二人組便以肩頸僵硬為由互相為對方按摩，並泡在浴缸裡聊著就寢時的保養等話題，時不時提供年輕的艾莉緹亞和珠洲菜作為參考，四人就這樣度過了沐浴時光。