「和您分別以後，我移居到了王都，住在下街，成天埋頭於製藥。瓦因蓋姆和洛基席瑪姆兩人，也一直在藥草採取上協助我，之後弟子和協力者增加，兩人便離去了，走上各自的路」

大家緊盯著導師之幻影。

亞馬密爾一級神官一句不漏地專注著。
全員都是第一次看到這個吧。

「培育了優秀的弟子們後，磨練了〈回復〉。然後，請感到高興吧，師阿。我終於，成功發動了〈淨化〉」

神官們，喔喔，地漏出了小小的感嘆聲。
恐怕對這些男人來說，能使用〈淨化〉，是究極的嚮往吧，雷肯想著。

「靠著那〈淨化〉，甚至得以服侍歷代的王陛下。得到了王宮的庇護，藥師的養成持續進展著，現在，就如同您所希望的，藥師的技術散佈到了国家的各個角落」

幻之斯卡拉貝爾導師的眼，在看著遠方。是在回憶走過的人生吧。

「瓦因蓋姆和洛基席瑪姆也很努力。現在，不論是多麼鄉下的城鎮，應該都不會沒有藥師。您的藥，現在充滿於国家之中」

有神官在啜泣。雷肯雖然覺得不是那麼令人感動的述懷，但斯卡拉貝爾這藥師就是有被尊敬，仰慕到這等地步吧。

「因為學會了〈淨化〉，得到了意想不到的長壽。然而最近〈淨化〉也變得沒有效果了。這魂魄重返天上，這軀體歸還於土的日子也近了吧」

身為諾瑪祖父的侯爵，也靠著〈淨化〉延長了十四年的壽命，但到了最後，〈淨化〉沒了效果。〈淨化〉也非萬能的。

「此時，毫無疑問是您製作的藥，出現在眼前了。聽了製作的藥師的事便發現，那毫無疑問是您。當時，我有了兩個願望」

接下來是重點。斯卡拉貝爾的目的是什麼。

「第一個，是想讓您看看我製作的藥。想讓您看看我積年累月之精進」

想出示藥。然後想知道希拉的反應。這是第一個願望。

「另一個，是關於您的身體狀況。分別時，您的身體狀況，絶對算不上好」

（那個，一定是演技喔。不能當真）

「聽說，您都緊閉在家，幾乎沒有外出」

（出去了。出去了。很有精神地跑來跑去）

「現在正是向您報恩的時候」

（等等。這傢伙，該不會）

「趕到您那裡，膜拜您的尊顏，施展〈淨化〉，只要能盡可能延長您的壽命的話，沒有事情能比這還讓人高興了」

（這傢伙，想對希拉施展〈淨化〉！）

「一天的效果恐怕會很淡，因此想在沃卡城滯留九天，為您施展〈淨化〉。只要能辦到，就那樣死在沃卡也無所謂。這才是，與我生涯最後的工作相符之事吧」

王都來的使者一行人，表現得感動不已。

但是，雷肯心中，颳起了暴風。

（要是被施了什麼〈淨化〉，身為不死者的希拉就會被消滅）
（會化為灰燼）
（得讓斯卡拉貝爾這老頭打消過來的念頭）
（不論如何都要）

「說不定，從這魔石發出的聲音，並沒有被您本人聽到。您是世間少有的天之邪鬼。現在正隱藏著身姿也說不定吶」

（這老頭，很懂希拉）

「不過，您不會前去遠方。應該會在附近某處，好好守望著情況才對」

（什麼？）
（不，那也說不定）
（希拉就是這種人）

「因此，不知道是哪一位的，希拉大人身邊的人阿。請把這魔石交給希拉大人吧。只要還有魔力，就能放出我的身姿和聲音無數次。必要的咒文就刻在箱子上」

（喔。真有趣的魔法）

「希拉大人。與您相見的日子，實在讓人期待不已。是這活不久的老人最後的願望。還請答應吧」

幻影突然消去了。
使者們對消失的幻影敬禮。

雷肯正使力讓腦袋運轉。
拚命地思考著，接下來，要如何說服使者們，讓斯卡拉貝爾打消過來的念頭。