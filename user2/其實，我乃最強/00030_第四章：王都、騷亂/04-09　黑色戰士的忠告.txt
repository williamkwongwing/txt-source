糟透了。
入學第一天的課上，我被捲入了讚賞的風暴旋渦中。
原本應該被認定為不及格生才是。

提早退學計畫大幅度衰退。

我為了打破現狀，而召開緊急會議。話雖如此，成員也只有我和分身。地點在宿舍後面的樹林裡。在結界中隔音完美。麗莎在自己房間裡待命。

「不行。放棄吧。」

「你明明是我的分身，這麼早就放棄了！」

「你本來就是個容易放棄的人！」

「你也是隔天上學的？能忍耐五年嗎！？」

「不不，不，本來是你不對吧？我不幹了。你給我善後。」

「如果你出席的話，就不會發生這種事了！嗯，以後就都你去吧。」

「不講理！」
「別抱怨了，笨蛋！」
「說笨蛋的才是笨蛋！」
「什麼啊！」
「什麼嘛！」

互相對罵的我們。仔細想想就會變成罵自己的笨蛋。
自問自答不可能從開始就進行建設性的討論。第一個注意到。

分身也察覺到了嗎？

「那麼，現在到底該怎麼辦呢？事到如今，即使我出面說『什麼都做不了（嘿嘿♪）』，也只會被認為是在開玩笑吧？」

「問題就在這裡啊……」

一旦實力得到認可，就算是胡亂偷工減料也會被識破是故意的，這才是關鍵。

「不管怎麼說，我只能表現出實力已經到極限了。剩下的就是周圍的人都在成長，我相對落後，只能順著這個方向走。」

「就算講課沒問題，如果分身去上實技課的話，實際上什麼也做不了？」

「雖然這個消息看上去很高興，但是最上級的班級似乎可以帶上自己擅長的魔法具。」

「是啊！」

貌似是直接忘記，驚訝的分身。不愧是我，只記得方便的事。真悲哀。

雖說如此，因為分身難以進行細微的操作，也有辛苦的部分，我們分配授課的時間，一邊對視一邊反復議論——。

「和今天一樣，星期一本體，明天是分身」

沒有課的日子跳過，星期四分身，星期五決定是我。
這個世界一周也是七天，星期一叫第一，星期日叫第七。在學院裡，週末的第六、第七是休息日，但一般來說只有第七是休息日。

「看來會拖很長時間……」分身嘀咕說道。

「最糟糕的情況是【因為人際關係感到疲憊而鬱悶】，只能堅持到底。」

「這樣不就好了嗎……」

還行吧。我也想那樣。給義父添麻煩了。

「總之，到前期的中間考查為止，要看情況啊。雖然我覺得很累，但請加油。」

我也會加油的。
最後還是讓緊張感降到最底層，對策會議結束了。
（譯註：テンション，緊張。）

「總覺得，好像忘了什麼。」

我的話，讓分身歪著頭了。

「什麼來著？」

「嗯……」

我懷著【挖喔挖喔】的感覺回想起今天。（譯註：はほわほわほわわん，挖喔挖喔。）

「「針！」」

同時呼喊的我們。對了，對了。瞄準萊斯的針的事。差點忘了。因為那傢伙，我的計畫落空了。

明天再調查證物。
（譯註：ブツ，物，常指犯罪時的證物。）

我為了提醒當事人，跑了一趟——。



★★★★★★



萊斯結束了放學後的自主練習。
淋浴後很舒暢，腳步很輕地朝接送用的馬車前進。

今天真是個好日子。
終於和一直憧憬的男人直接相對了。而且是五年前再現的體術戰。

雖然結果只是顯示出了力量的差距，但他還是很滿足。

變化得連自己都吃驚。
如果是以前——如果是五年前的話，就和那時一樣，無法接受現實，只是不甘心，因為憎恨對方、嫉妒之火而焦慮吧。

但是不知不覺憎恨和嫉妒變成了羨慕，變得追求強大。

——總有一天，想要追上那個背影。

雖說如此，但也有反省。

（結果今天也沒能問……）

母親的嚴格命令。
芬菲斯卿領內活動的身份不明的黑戰士的情報，錯過了從哈特得到的時機。
上課時被大家所忌憚，下課後哈特馬上消失了。

差不多該拿出點成績了。
心情沉重。很抱歉可能會捲入這奇妙的糾紛中。

一轉腳步變得沉重，終於到達了王族專用的箱型馬車。從車夫打開的門進去，
（譯註：御者，運転手，車夫。）

「辛苦了。你好慢啊。」

「為什麼姐姐在呢？」

姐姐瑪麗安輕輕地坐在座位上。

「偶爾也不錯吧。現在我不能進入離宮了。」

「有話在學校內說也可以。」

倒不如那樣做。
門被關上，和瑪麗安娜空出距離坐著。一個車夫從正面的小窗戶裡窺視著其中。

他們是母親王妃指定的人。監視著長官不能秘密談話。

馬車轟隆轟隆地前進。

萊斯被搭話了也坦率地回答，馬上結束話題。

走出校外，過了一會談話也中斷了，無聲地聽著車輪的聲音。

——聲音停止了。

然後眼前的座位上，突然出現了一個全身黑衣的男人。

「你是什麼人？」
「怎麼進來的！？」

空間轉移？除此以外不能考慮。
黑衣男舉起一隻手制止了兩人。

「很抱歉讓你們受驚了。我是『濕婆』。是正義的執行者。」

對複數像重疊了一樣的聲音，萊斯嚇了一跳。
雖然不知道他的名字，但肯定是芬菲斯卿領內暗中活動的黑色戰士。沒想到本人會出現在眼前……。

「那麼，我們簡短地談一下事情吧。」

兩個人咕嘟咕嘟的喉嚨一響，黑衣男淡淡地說道。

「萊斯王子，你被盯上了。」

「哈？」

「光是今天就有兩次攻擊瞄準你飛去。」

男人舉起手。兩個手指好像抓住了什麼。凝神仔細一看，

「……針嗎？」

像頭髮那麼細，像金屬製的針一樣的東西。

「在與同學一起上實技課的時候，好像瞄準了你疏忽大意的瞬間。犯人還沒抓到。目的不明。你有線索嗎？」

對下任國王的自己感到厭煩的人，在王國有很多。在王與王妃的對立中，想要加深鴻溝的勢力就在那裡。

「太過常見了，不清楚。」

突然覺得黑衣男笑了。他的手放下來的時候，針消失在某處。

「話就這些。啊，今後在學校裡也請留意身體。我不一定總是在你身邊。」

「這次，你碰巧遇見了，我得救了嗎？」

「啊啊，但是不必道謝。只是順其自然。雖然沒有義務幫助你，但是眼前沒有罪的人受傷是不能置之不理的。反正我是正義的執行者。」

擺出一副奇怪的姿勢的黑衣男。

「先說聲謝謝。順便想問很多問題。你是什麼人？來王都做什麼？」

「不要去探索。希望你不要把我的事情掛在嘴邊。」

「就算被說了不要說出口……」

看了男人的背後。在小窗的對面，有人側耳傾聽箱型馬車裡的對話。

「啊，聲音不會洩漏到外面。外面的聲音也聽不到吧？」

確實，在男子出現後，馬車前進的聲音也聽不到了。

「再見了。」

「等一下！那個……其實我被要求去找你。」

為什麼自己要老實地說傻話？
還他救了我的恩情？不，既然沒有確鑿的證據，就沒有那個。

只是，一直顫抖不止。
本能告訴自己，無論做什麼都敵不過這個男人。【他】就好像母后一樣。

「找我？誰呢？」

這是理所當然的問題。如果老實地回答的話，很可能會成為焚燒王都的火種。

「是母親大人……」

但是，我不能不說。

「啊，是那個女人啊。那就沒關係了。」

「啊？」

「我和那傢伙有緣。是注意到我的行動了嗎？那是當然的反應。不過嘛，她還是讓學生身份的兒子去做吧。」

總覺得語氣有點崩潰了。

「啊，可以嗎？」

「那傢伙什麼都做不了。」

脊樑發冷。他對王國最強的閃光公主，自信滿滿的口吻確信沒有謊言。

「反正我不打算給你我的資訊。今天遇到的事情可以告訴你。啊，但是還是不要告訴她本人說是找我比較好哦？那傢伙很害怕吧？」

總覺得精神上的距離接近了，萊斯的顫抖也不知不覺中止了。

「那麼就這樣，再見了！」

剛一擺出讓雙手交叉的奇怪姿勢，男人就像融化在空氣中一樣消失了。

吱，行駛中的門開了。不久，門咯吱一聲關上了。

「出去了，是嗎……？」

「大概吧……」

「那麼，出現的時候也不是空間轉移……」

恐怕男人是萊斯進馬車時，就消失身影偷偷上車的吧。

「就算是完全消失了，也不可能……」
「是啊……」

車輪的聲音又回來了。
兩個人聽著馬車嘰裡咕嚕地前進的聲音——。