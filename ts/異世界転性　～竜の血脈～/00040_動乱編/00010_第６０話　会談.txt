阿，馬上又見面了。

莉雅在城堡走廊上看見了穿著白色披風的卡拉，接著出聲叫了對方。

「嗨，要去哪阿？」
「嗯，在公主準備好之前，打算去閱兵順便進行訓練吧」

卡拉爽快的回答著。眼前這完美無瑕的容貌，沒想到竟然曾在自己懷裡崩潰過。
一這樣想就讓莉雅胸中感到一陣苦悶，心中『邪惡』的念頭已經快充滿整個人而外溢出來了。

「這樣的話我可以一起去嗎？」
「當然可以，這邊請」

卡拉露出完美的笑容回答著。

就這樣兩個人站在一起看著騎士們訓練的情形。
偶而會有騎士前來尋求建議，卡拉都會一一回答他們。
似乎卡拉被迷住的騎士恍惚的點點頭後便回去進行訓練了。
卡拉臉上帶著微笑，隨著她的視線掃過，訓練狀況也變的更加熱絡。

很神奇的是沒有任何人用帶有惡意的視線看著她，大概是他們都覺得自己沒有資格跟女神談戀愛吧。
嘛，如果訓練中滿腦子都是這種事情，反而很容易引起長官注意吧。
而且就算是卡拉也是需要進食跟排泄的。

⋯⋯這是正常人都有的行為。

正當莉雅想著這些沒禮貌的事情時，一名騎士從卡拉面前經過後站在莉雅正前方。

「殿下，可以打擾一下嗎？」

站的非常挺立，像是那種一本正經的武者一樣。

「嗯，什麼事？」
「聽說殿下使用武士刀的技巧非常高超，實際上敝人也使用著武士刀，還請您能與敝人進行實際對戰指導」

而這名騎士的腰上確實掛著武士刀。

「好阿」

莉雅覺得這個人的感覺還不錯，所以就依照他的期望進入了修練場。
一拔出刀後，莉雅便迅速的集中精神在戰鬥上，她就是這種戰鬥生物。

雖然說要求指導，但是實際交手後發現，對方的技術其實很不錯。

「很強呢」

卡拉開口向回到觀戰區的莉雅搭話。

「實際對戰過後，才知道對方的實力」
「不過從旁邊觀戰時，感受完全不一樣」

原來如此，或許是這樣也不一定。
卡拉看起來就是個冰山美人，不過實際觸摸後確實是有體溫的。
但是為什麼呢。
莉雅手上一沒握著刀後，又再度開始胡思亂想中。

這個，應該是那個。
這大概就是那種感情。
被人稱為『戀愛』的感情。

───

「那麼，讓我們好好的聊聊天吧」

啪！吉妮維亞拍了一下手後這麼說著。那些無用的開場白全部省略，這句話正式宣告著會議開始。

現場除了有瑪內夏的國政大臣外，還有莉雅一行人。雖然說是一行人，但是作為奧加與獸人代表的紀咕跟瑪露他們在一旁等著，他們的事要等之後有空在一一解決。
也許應該要把薩基一起帶來才對，但讓這麼年輕的小孩擔任軍師一職似乎不太妥當。

「那麼首先這場會議的目的，是希望集結大陸西北方的勢力，一起想辦法渡過千年記，是這樣對吧？」

莉雅點點頭。

「就是這樣，此外在這個過程中，要毀滅科爾多巴或是使其屈服，進而改變政治及法律制度」
「改變嗎？」
「首先就是進行修法，這個國家對於犯罪者過於嚴苛了」

莉雅所說的科爾多巴的法律，不光只是過於嚴苛，同時執行面上也過度僵化。
法律審判時喜歡引用以前的案例做為依據，完全沒有依照個案酌情考量的餘地。此外官員隨意告發民眾觸犯內亂罪，使的百姓對於政府連發牢騷都不行。
這根本就是對國民進行思考統治，以專制國家而言完全是屬於性質惡劣的那種。

莉雅想著果然還是該讓薩基一起出席才對，因為他讀過大量的書籍，所以可以提出多種不同方案來參考。之後再找他單獨商量一下好了。

「還有，不光只有科爾多巴而已，也要消除其他地方的人們對於亞人的差別待遇。但這點恐怕不容易達成」

事實上這一帶的獸人人口數量如果認真統計的話，說不定會比人類還多。
而這麼龐大數量的獸人卻沒有建立起自己的國家反而讓人感到意外。他們多半習慣狩獵生活也是原因之一，此外不論前世或這個世界，多半是因為農業興盛而導致人口密集，進而形成城鎮。

「亞人差別待遇嗎⋯⋯這也是問題之一呢。不過還好我國沒有特別排斥亞人的狀況」

但是周圍仍有國家是採取明確區分人種的政策。
有些是因為生活方式不同的原故，有些則是抱持著雙方無法共存的想法，也有一些統治者是因為自己心裡無法接受獸人而採取種族階級制度的笨蛋。

「但是食物來源以及領地糾紛沒解決的話，只會產生更多的衝突」

吉妮維亞聳了聳肩膀，不過這些問題應該有在處理吧？
國務大臣見狀點了點頭。

「那是未來的問題，而現今最重要的還是眼前的千年記。此外糧食問題的部分，看來有必要宣導農業的重要性了。只是獸人跟奧加們都偏好吃肉⋯⋯」

此時感受到瑪露跟紀咕在自己背後點頭的感覺。

「嘛，糧食問題總有辦法可以解決。說實話我想要的是矮人的鍛造技術以及迷宮出產的魔石，只要有了這兩樣的話，我有自信即使只有我國也可以單獨擊敗科爾多巴」

吉妮維亞提的是她所乘坐的那個巨大魔像量產計畫。
科爾多巴軍事上最著名的是施展了防護魔法的重裝步兵，以及擁有機動力跟衝擊力的騎兵。但是考量到戰爭規模擴大後的佔領與支配問題，前面那些都不算是最嚴重的威脅。

「要打贏科爾多巴，就必須擊敗他們的工兵以及補給線」

科爾多巴最大的優勢是在於它是一個高度戰爭特化的國家。
國內的道路整備、要塞建築、裝備糧草運輸以及軍隊移動等。
這些功能本身就是一大威脅。

「所以說，要擊敗科爾多巴果然還是得組成大聯盟阿」

外務大臣跟情報大臣一一報告著，雖然有不少國家跟科爾多巴是敵對狀態，但也有一些國家是採取親科爾多巴的政策。
而這兩者的共通點都是，他們對科爾多巴感到恐懼。
即使是親科爾多巴的國家，他們也只是因為害怕而選擇服從，如果可以的話他們會衷心期望科爾多巴毀滅或是弱化。

「因此，要建立起反科爾多巴的大型聯盟，那個掌旗者就是妳呦」

吉妮維亞指著莉雅說著。

「我嗎？」
「因為妳是『卡薩莉雅』的公主，是科爾多巴『侍奉的主家』的公主」

吉妮維亞一邊用著奇怪的節奏說著手指一邊轉著圈圈。

「如果是聽從於妳，『就能聚集其他國家』了」

吉妮維亞接著停頓了一下。

「妳有這個覺悟嗎？」

這樣的談話讓莉雅有點尷尬。
因為說到掌旗者這件事，莉雅自己已經有經驗了。

說起來奧加全部的部族都已經納入莉雅管轄了，奧加的戰士們全都可以為了莉雅而死。
如果是跟他們一起戰鬥的話，即使是長驅直入戰場中心也無所謂。但一下子就要背負起這些陌生國家的百姓性命，這難道不會太過傲慢嗎。

「從妳的卡薩莉雅老爸那要求一個大公爵的爵位，成立一個大公國，以此對抗科爾多巴如何？」

原來如此，這是吉妮維亞所思考的戰略阿。

「我不知道如何經營國家⋯⋯」
「這可以交給我，由我來攝政就好。實際上就是讓瑪內夏直屬於公國」

可是這樣一來瑪內夏王國就等同滅亡了吧。

「然後我的兒子過給妳當養子，這樣一來也能保存住瑪內夏的正統性」

原來如此，打算這樣做阿。

「而且還希望妳能從我底下的貴族中選一個人當配偶，但是⋯恕我直問，聽說您是同性愛好者？」

聽到女王這麼直率的發問，大臣們無不緊張了起來。

『這個女王還真是大喇喇的問了阿』莉雅這麼想著，不過這裡並不打算說謊。
莉雅誇張的深呼吸然後縮著肩膀回答著。

「是真的，打從出生到現在，從來沒有產生過讓男性擁入懷中的感情。」

對於莉雅果斷的回答，大臣們不禁愕然的看向她。
這種事情一般都會故意隱瞞起來才對。不過，其實也有一個非常有名的男同性愛的貴族存在，所以這邊才會據實以告吧。

「長的像美少女的美少年也不行嗎？」
「沒辦法，卡薩莉雅皇宮裡也有很多美少年，但還是只能接受女性」

這堅定的同性愛宣言引起閣員們一陣厭惡。
莉雅很好奇卡拉是怎麼看待這件事的，但是她像是什麼都沒發生一樣，就這樣站在女王身後。

「嘛，如果是為了政治目的一定要安排一個人跟我結婚也不是不行，但我是很喜歡夜間生活的人，所以還不如幫我安排個新娘如何？」

由於這個發言實在太過露骨，所以讓不少新手大臣們臉紅了起來。
這個國家的高階大臣在組成上顯得過於年輕，那種久經歷練的大臣很少。

「新娘嗎⋯⋯剛好有這樣的人選呢」

女王延續著這個奇怪的話題。

「必須是高階貴族的女兒，此外未婚，而且容貌要好看⋯⋯」

她的視線轉向背後。

「卡拉，要不要去？」

在這一瞬間，議場整個沸騰了起來。

「不、不行不行！」「卡拉殿下的話不行！」「卡拉大人是大家的！」「乾脆陛下自己嫁過去比較好！」

卡拉意外的非常有人氣阿，不過她是救國的聖女所以會這樣也很正常。反而是女王陛下意外的沒人望阿。
雖然如此，但由於各大臣彼此互相扯後腿的關係，所以卡拉至今都還沒嫁人。這對莉雅而言是Good Job！

「這樣的話，卡拉大人有中意的男人⋯⋯」「⋯⋯男人？」「沒有這種男人吧⋯⋯」「⋯⋯」

為什麼講到這點就安靜了下來阿，大臣們。

「⋯⋯沒有對吧？」「嗯，跟男人相比之下，還不如選莉雅殿下」「這是對我最大的讚美阿」
「決定拉！！」

喂喂，用這個充滿氣勢的喊話就想把事情呼嚨過去嗎。
宰相清了清嗓子後向女王報告。

「所以總而言之，臣等對於公主您的決定並不反對，但是⋯⋯」

全場的視線都集中在卡拉身上。
聖女完全沒有改變，臉也沒有通紅，帶著微笑回答著。

「如果這是殿下的意願的話」

───

接下來是政治跟外交的舞台。

首先為了要能最大限度的利用卡薩莉雅的權威，向王都派出了使者。
除了閣員級的大臣擔任大使外，還派了卡洛斯陪同，要將旅途中詳細的經歷傳達給父王知道。

此外露露也跟著同行，這是為了向安卡莎說明事情經過。（譯註：安卡莎是莉雅的母親，我猜很多人已經忘記她了，所以特別說明一下）

原本應該要讓紀咕擔任奧加的代表一起去才對，但是這邊最後決定不派他去，因為他根本不懂政治相關的事情，所以奧加代表打算路過奧加村落的時候再另外挑人擔任。

此外，獸人代表也是一樣打算在半路上找合適的人選。
如果莉雅可以親自回去一趟的話當然是最好的，但是她必須跟這邊的貴族們會面，跟其他國家的大使談判，需要她本人露臉的工作堆積如山。

「要好好注意身體歐。卡洛斯，如果敢對露露伸鹹豬手，我會宰了你」
「以我身為騎士的榮譽發誓」

卡洛斯微微顫抖著發誓。

就這樣那兩人離開了瑪內夏。

───

「寂寞嗎？」

莉雅身後的希茲娜開口問著，仔細想想的話，這兩人是莉雅這趟旅行中最初的夥伴。
莉雅帶著笑容轉過身去。

「很寂寞呦。所以可以安慰我嗎？」

希茲娜紅著臉溫柔的抱住莉雅。

莉雅跟同伴們搬到了卡拉在城堡內的宅邸，也因此跟她碰面的機會變少了。
因為她身為女王親衛隊隊長及秘書的關係，所以工作非常忙碌。平常幾乎都是直接睡在後宮。而莉雅自己則是在瑪內夏國內各處奔波。
薩基不在的時間也變多了，他協助女王開發著魔導兵器。但也從吉妮維亞那學到不少施展魔法的技術。
伊琳娜則是去了騎士團那，在那邊接受初級劍術的指導。所以因此離開了莉雅的身邊，也因為她不願意跟瑪露分開的關係，所以瑪露也暫時跟過去了。
因此只剩下希茲娜跟紀咕會經常跟莉雅一起行動⋯⋯

莉雅跟希茲娜的寢室是同一間。
希茲娜名義上掛的是莉雅的護衛，但實際上是她的愛人。
不過她有參加過騎士團的訓練，在展露過自身的實力後，已經很少有人會用輕視的眼神看她了。但她一想到自己成為了莉雅的戀人就會感到害羞。

這是她的初戀。
跟莉雅相會後，不知道為什麼會愛上她。說起來自己本來並不是愛好此道的人。

雖然她想了很多，但是莉雅並沒有每天晚上都要。
偶而有餘裕的時候也只是親親臉頰，說說甜言蜜語然後就入睡了。

平穩的日子持續進行著。
因為知道了卡拉跟莉雅還沒同床，所以希茲娜放心了。正妻的座位是卡拉的了，這是因為身分差距的問題所以無可奈何。

雖然對於卡拉的美貌而自卑，但是莉雅現在抱著睡的人是我，希茲娜如此自我安慰著。
卡拉在確定會成為莉雅妻子的時候，莉雅曾經說過。

「因為我們不是真的成為夫妻，所以如果遇到了喜歡的男人時，就把我捨棄掉吧」
「這是不可能的吧」

卡拉面帶溫柔的笑容斷言著。

───

直到那天夜晚。

悄悄溜出城堡的莉雅來到了市街區域。
莉雅這麼做並沒有明確的目的，有目的的是來者。

深沉的黑夜中，一隻蝙蝠飛了過來。
站在小巷子裡的是，批著頭巾有著亞麻色頭髮的少女。

「光是進到城市裡就已經很困難了耶，就不能讓結界稍微放鬆一點嗎」

明日香一邊抱怨著一邊說著。

「科爾多巴有動作了」

動亂就此展開了。